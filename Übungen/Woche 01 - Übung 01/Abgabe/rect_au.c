#include <stdio.h>

int main(int argc, char const *argv[])
{
    double length, width = 0;
    printf("Type in length:\n");
    scanf("%lf", &length);

    printf("Type in width:\n");
    scanf("%lf", &width);

    printf("Area = %.2lfcm²\n", width * length);
    printf("Extent = %.2lffcm\n", (2 * width) + (2 * length));
    return 0;
}
